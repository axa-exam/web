﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AXA.Exam.Web.Models {
    public class Schedule {
        public string Date { get; set; }
        public string Time { get; set; }
        public string ApiKey { get; set; }
    }
}
