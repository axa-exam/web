﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AXA.Exam.Web.Models {
    public class FileUpload {
        public string File { get; set; }
        public string ApiKey { get; set; }
    }
}
