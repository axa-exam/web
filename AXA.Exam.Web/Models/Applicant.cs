﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AXA.Exam.Web.Models {
    public class Applicant {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string PositionApplied { get; set; }
        public string Source { get; set; }
    }
}
