import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent {
  private http: HttpClient;
  private baseUrl: string;
  public name = new FormControl();
  public email = new FormControl();
  public mobile = new FormControl();
  public position = new FormControl();
  public source = new FormControl();
  public formdata: FormGroup;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.http = http;
    this.baseUrl = baseUrl;
  }
  getFormControl(name) {
    return this.formdata.get(name);
  }
  ngOnInit() {
    this.formdata = new FormGroup({
      name: this.name,
      email: this.email,
      mobile: this.mobile,
      position: this.position,
      source: this.source
    });
  }

  onSubmit() {
    if (!this.formdata.valid) return;

    var body = {
      name: this.name.value,
      email: this.email.value,
      mobile: this.mobile.value,
      positionApplied: this.position.value,
      source: this.source.value,
    };

    this.http.post<any>(this.baseUrl + 'registerAPI', body).subscribe(result => {
      var isOk = result.result == 200;
      console.log(result);
      Swal.fire({
        icon: isOk ? 'success' : 'error',
        title: isOk ? 'Success!' : 'Error',
        text: isOk ? result.message : result.errMessage
      })

      if (isOk) {
        this.formdata.reset();
      }
      
    }, error => console.error(error));
  }

}
