import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css'],
})
export class UploadComponent {
  private http: HttpClient;
  private baseUrl: string;
  private filebase64: string;
  public apiKey = new FormControl();
  public file = new FormControl();
  public formdata: FormGroup;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.http = http;
    this.baseUrl = baseUrl;
  }
  getFormControl(name) {
    return this.formdata.get(name);
  }
  ngOnInit() {
    this.formdata = new FormGroup({
      apiKey: this.apiKey,
      file: this.file
    });
  }

  handleUpload(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.filebase64 = reader.result.toString();
    };
  }

  onSubmit() {
    if (!this.formdata.valid) return;

    var body = {
      apiKey: this.apiKey.value,
      file: this.filebase64
    };

    this.http.post<any>(this.baseUrl + 'uploadAPI', body).subscribe(result => {
      var isOk = result.result == 200;
      console.log(result);
      Swal.fire({
        icon: isOk ? 'success' : 'error',
        title: isOk ? 'Success!' : 'Error',
        text: isOk ? result.message : result.errMessage
      })

      if (isOk) {
        this.formdata.reset();
      }

    }, error => console.error(error));
  }

}
