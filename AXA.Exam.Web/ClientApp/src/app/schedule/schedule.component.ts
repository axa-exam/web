import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';
import * as moment from 'moment';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css'],
})
export class ScheduleComponent {
  private http: HttpClient;
  private baseUrl: string;
  public date = new FormControl(new Date());
  public time = new FormControl(new Date());
  public apiKey = new FormControl();
  public formdata: FormGroup;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.http = http;
    this.baseUrl = baseUrl;
  }
  getFormControl(name) {
    return this.formdata.get(name);
  }
  ngOnInit() {
    this.formdata = new FormGroup({
      date: this.date,
      time: this.time,
      apiKey: this.apiKey,
    });

    
  }

  public disabledDates = (date: Date): boolean => {
    var today = new Date();
    return date.getDate() < today.getDate();
  }

  onSubmit() {
    if (!this.formdata.valid) return;

    var body = {
      date: moment(this.date.value).format('yyyy-MM-DD'),
      time: moment(this.time.value).format('hA'),
      apiKey: this.apiKey.value,
    };

    this.http.post<any>(this.baseUrl + 'scheduleAPI', body).subscribe(result => {
      var isOk = result.result == 200;

      Swal.fire({
        icon: isOk ? 'success' : 'error',
        title: isOk ? 'Success!' : 'Error',
        text: isOk ? result.message : result.errMessage
      })

      if (isOk) {
        this.formdata.reset();
      }
      
    }, error => console.error(error));
  }

}
