﻿using AXA.Exam.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace AXA.Exam.Web.Controllers {
    [ApiController]
    [Route("[controller]")]
    public class ScheduleAPIController : ControllerBase {
        private readonly ILogger<ScheduleAPIController> _logger;

        public ScheduleAPIController(ILogger<ScheduleAPIController> logger) {
            _logger = logger;
        }

        [HttpPost]
        public object Post([FromBody] Schedule schedule) {
            var client = new HttpClient {
                BaseAddress = new Uri("https://localhost:44300/")
            };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
            client.DefaultRequestHeaders.Add("x-axa-api-key", schedule.ApiKey);

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "schedule") {
                Content = new StringContent(JsonSerializer.Serialize(new {
                    ProposedDate = schedule.Date,
                    ProposedTime = schedule.Time,
                    Online = "true"
                }), Encoding.UTF8, "application/json")//CONTENT-TYPE header
            };
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return client.SendAsync(request).Result.Content.ReadAsStringAsync().Result;
        }

    }
}
