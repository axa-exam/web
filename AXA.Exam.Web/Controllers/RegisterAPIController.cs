﻿using AXA.Exam.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace AXA.Exam.Web.Controllers {
    [ApiController]
    [Route("[controller]")]
    public class RegisterAPIController : ControllerBase {
        private readonly ILogger<RegisterAPIController> _logger;

        public RegisterAPIController(ILogger<RegisterAPIController> logger) {
            _logger = logger;
        }

        [HttpPost]
        public object Post([FromBody] Applicant applicant) {
            var client = new HttpClient {
                BaseAddress = new Uri("https://localhost:44300/")
            };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "register") {
                Content = new StringContent(JsonSerializer.Serialize(applicant), Encoding.UTF8, "application/json")//CONTENT-TYPE header
            };
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return client.SendAsync(request).Result.Content.ReadAsStringAsync().Result;
        }

    }
}
