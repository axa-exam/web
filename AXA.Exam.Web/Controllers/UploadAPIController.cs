﻿using AXA.Exam.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace AXA.Exam.Web.Controllers {
    [ApiController]
    [Route("[controller]")]
    public class UploadAPIController : ControllerBase {
        private readonly ILogger<UploadAPIController> _logger;

        public UploadAPIController(ILogger<UploadAPIController> logger) {
            _logger = logger;
        }

        [HttpPost]
        public object Post([FromBody] FileUpload file) {
            var client = new HttpClient {
                BaseAddress = new Uri("https://localhost:44300/")
            };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("x-axa-api-key", file.ApiKey);

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "upload") {
                Content = new StringContent(JsonSerializer.Serialize(new {
                    file = new {
                        mime = "application/pdf",
                        data = file.File
                    }
                }), Encoding.UTF8, "application/json")
            };

            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return client.SendAsync(request).Result.Content.ReadAsStringAsync().Result;
        }

    }
}
